# -*- coding: utf-8 -*-

import uuid
import unittest
import mock
from django.utils import timezone
from django.conf import settings
from django_merceariadigital import models, controllers


class OrderControllerTest(unittest.TestCase):
    def setUp(self):
        request = mock.Mock()
        self.controller = controllers.OrderController(request)
        now = timezone.now()
        od = {'init_date': now,
              'end_date': now + timezone.timedelta(days=60),
              'value': 150, 'token': str(uuid.uuid4),
              'success': True,
              'user_token': '123qweasdzxc'}

        self.order = self.controller.create(**od)

        response = """{
"errors":[
{
"code":"CÓDIGO_DO_ERRO",
"message":"MENSAGEM_DO_ERRO"
}
],
"order":{
"identifier":"IDENTIFICADOR_DO_PEDIDO",
"number":"%s"
},
"payment":{
"identifier":"IDENTIFICADOR_DO_PAGAMENTO",
"amount":10
},
"success":"STATUS_DA_TRANSAÇÃO"
}
""" % self.order.token
        self.controller.request.REQUEST = {
            'transactionResponse': response,
            'transactionMid': settings.TRANSACTION_MID}

    def tearDown(self):
        models.Order.objects.all().delete()

    def test_create_order_without_required_params(self):
        """ Ensure that a Order can't be created without required params.
        """
        user = mock.Mock()
        with self.assertRaises(Exception):
            self.controller.create(user)

    def test_create_order(self):
        """ Ensure that a Order can be created properly
        """
        now = timezone.now()
        od = {'init_date': now,
              'end_date': now + timezone.timedelta(days=60),
              'value': 150, 'token': str(uuid.uuid4),
              'success': True,
              'user_token': 'asdf123zxv'}

        order = self.controller.create(**od)
        self.assertTrue(order.id)

    def test_handle_merceariadigital_return(self):
        """ Ensure that the return from mercearia digital is handled correctly.
        """
        self.assertTrue(self.controller.handle_mercearia_digital_return())

    def test_show_payment_post_form(self):
        """ Ensure that the form for payment on merceariadigital is rendered
        properly.
        """
        user_info = {'email': 'a@a.com', 'name': u'zé mané',
                     'identity': 'asfd123'}
        form = self.controller.show_payment_post_form(
            self.order, user_info).content
        self.assertIn(settings.MERCEARIA_DIGITAL_PAYMENT_ACTION, form)
