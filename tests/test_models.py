# -*- coding: utf-8 -*-

import unittest
from uuid import uuid4
from datetime import timedelta
from django.utils import timezone
import mock
from django_merceariadigital import models


class OrderTest(unittest.TestCase):
    def tearDown(self):
        models.Order.objects.all().delete()

    def test_create_order_without_required_fields(self):
        """ Ensure that an order can't be created without required fields
        """
        order = models.Order()
        with self.assertRaises(Exception):
            order.save()

    def test_create_order(self):
        """ Ensure that an order can be created properly
        """
        init_date = timezone.now()
        end_date = init_date + timedelta(days=30)
        value = 100
        order = models.Order(init_date=init_date, end_date=end_date,
                             value=value)
        order.user_token = str(uuid4())
        order.save()
        self.assertTrue(order.token)

    def test_process_return_dict(self):
        """ Ensure that a return dict from mercearia digital is processed
        correctly
        """
        init_date = timezone.now()
        end_date = init_date + timedelta(days=30)
        value = 100
        user_token = str(uuid4())
        order = models.Order(init_date=init_date, end_date=end_date,
                             value=value, user_token=user_token)
        order.save()

        returned_dict = {'order': {'number': order.token}}

        models.Order.process_return_dict(returned_dict)
        order = models.Order.objects.get(id=order.id)

        self.assertTrue(order.success)
