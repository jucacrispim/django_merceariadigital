# -*- coding: utf-8 -*-

import json
from django.utils import timezone
from django.conf import settings
from django.http import HttpResponse, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response, render
from django.core.validators import validate_email
from django.contrib.auth import authenticate
from django.utils.translation import ugettext as _
from django_merceariadigital.models import Order


class BaseController(object):

    def __init__(self, request):
        self.request = request

    def _return_error_json(self, msg):
        """ Returns a json with a http status 500
        :param msg: mesage to user
        """
        err = {'status': 500,
               'msg': msg}
        return HttpResponseServerError(json.dumps(err),
                                       content_type='application/json')

    def _return_json(self, msg):
        """ Returns a json with a http status 200
        :param msg: mesage to user
        """

        ret = {'status': 200,
               'msg': msg}
        return HttpResponse(json.dumps(ret),
                            content_type='application/json')


class OrderController(BaseController):
    """ Controller that handle orders.
    """

    def create(self, **order_data):
        """ Create an order, using mercearia digital.
        :param **order_data: Order data.
        """

        order = Order(**order_data)
        order.save()
        return order

    def handle_mercearia_digital_return(self):
        """ Handle a response from mercearia digital.
        """
        response = self.request.REQUEST.get('transactionResponse')
        myjson = json.loads(response)
        transaction_mid = self.request.REQUEST.get('transactionMid')

        if transaction_mid != settings.TRANSACTION_MID:
            raise Exception('Wrong transactionMid!')

        if not myjson.get('success'):
            raise Exception('Operation not well succeded')

        Order.process_return_dict(myjson)

        return True

    def get_payment_form_context(self, order, user_info):
        """ Returns the context for a given order
        :param order: Order to be posted by form.
        :param user_info: dict containing 3 keys: 'name', 'email' and
        'identity', an user's token
        """

        transaction_mid = settings.TRANSACTION_MID
        campaign_id = settings.CAMPAIN_ID
        country = settings.COUNTRY
        currency = settings.CURRENCY
        action = settings.MERCEARIA_DIGITAL_PAYMENT_ACTION
        order_amount = '{:.2f}'.format(float(order.value))
        order_amount = int(order_amount.replace('.', ''))


        myjson = {'merchantId': transaction_mid,
                  'campaignId': campaign_id,
                  'customer': user_info,
                  'order': {'number': order.token},
                  'payment': {'amount': order_amount,
                              'country': country,
                              'currency': currency}}

        myjson = json.dumps(myjson)
        context = {'transaction_mid': transaction_mid,
                   'transaction_request': myjson,
                   'merceariadigital_payment_action': action,}
        return context

    def show_payment_post_form(self, order, user_info, **extra_context):
        """ Shows the payment form - that will be posted onload - for a
        order.
        :param order: Order to be posted by form.
        :param user_info: dict containing 3 keys: 'name', 'email' and
        'identity', an user's token
        :param extra_context: context to template
        """
        context = self.get_payment_form_context(order, user_info)
        context.update(extra_context)
        return render(self.request,
            'merceariadigital/paymentform.html', context)
