# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('init_date', models.DateTimeField(verbose_name='Data inicial')),
                ('end_date', models.DateTimeField(verbose_name='Data final')),
                ('value', models.DecimalField(verbose_name='Valor', max_digits=6, decimal_places=2)),
                ('token', models.CharField(max_length=64)),
                ('success', models.BooleanField(default=False)),
                ('user_token', models.CharField(max_length=64)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
