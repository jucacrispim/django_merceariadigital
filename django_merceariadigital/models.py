# -*- coding: utf-8 -*-

import datetime
from django.utils import timezone
from uuid import uuid4
from django.db import models
from django.utils.translation import ugettext as _


class Order(models.Model):
    init_date = models.DateTimeField(_('Initial date'),
                                     null=False, blank=False)
    end_date = models.DateTimeField(_('Final date'),
                                    null=False, blank=False)
    value = models.DecimalField(_('Value'), max_digits=6, decimal_places=2,
                                blank=False, null=False)
    token = models.CharField(max_length=64)
    success = models.BooleanField(default=False)
    user_token = models.CharField(max_length=64)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = str(uuid4())

        super(Order, self).save(*args, **kwargs)

    def __unicode__(self):
        dtformater = lambda d: datetime.datetime.strftime(d, '%Y/%m/%d')
        uni = unicode('%s - %s | %s' %(dtformater(self.init_date),
                                       dtformater(self.end_date),
                                       self.value))
        # béééé
        return uni

    @classmethod
    def process_return_dict(cls, returned_dict):
        """ Process the returned json (here cames as a dict) from mercearia
        digital for a given order.

        :param returned_dict: returned json (already load()'ed) from mercearia
        digital
        """
        order = cls.objects.get(token=returned_dict['order']['number'])
        order.success = True
        order.save()
        return order
