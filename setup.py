# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

VERSION = '0.2'
DESCRIPTION = """
Integration between Django and Mercearia Digital
"""
LONG_DESCRIPTION = DESCRIPTION

setup(name='django_merceariadigital',
      version=VERSION,
      author='Juca Crispim',
      author_email='juca@poraodojuca.net',
      url='',
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
      packages=find_packages(exclude=['tests', 'tests.*']),
      include_package_data=True,
      install_requires=['django>=1.7'],
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Web Environment',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License (GPL)',
          'Natural Language :: English',
          'Operating System :: OS Independent',
      ],
      test_suite='tests',
      provides=['django_merceariadigital'],)
